require 'rspec'
require 'rspec/its'
require './tennis_game.rb'

describe Tennis::Game do

  subject(:game) { Tennis::Game.new }

  before do
    game.input = StringIO.new
    game.output = StringIO.new
  end
  
  context 'initially' do
    its(:total_score_description) { is_expected.to be_nil }
    its(:highest_score) { is_expected.to eq(0) }
    its(:lowest_score) { is_expected.to eq(0) }
  end

  context 'just started' do
    before { game.start }
    its('output.string') { is_expected.to include('The game has begun') }
    its('output.string') { is_expected.to include('love-all') }
  end

  context 'using #point_won_by(player)' do
    before { game.start }

    it 'should increment score' do
      2.times { game.point_won_by('McEnroe') }
      expect(game.output.string).to include('McEnroe: 2')
    end
          
    it 'should recognize McEnroe win' do
      4.times { game.point_won_by('McEnroe') }
      expect(game.output.string).to include('McEnroe: 4')
      expect(game.output.string).to include('win')
    end

    it 'should recognize Federer win' do
      4.times { game.point_won_by('Federer') }
      expect(game.output.string).to include('Federer: 4')
      expect(game.output.string).to include('win')
    end
    
    it 'should recognize deuce score' do
      2.times { game.point_won_by('Federer') }
      3.times { game.point_won_by('McEnroe') }
      2.times { game.point_won_by('Federer') }
      expect(game.output.string).not_to include('win')
      expect(game.output.string).to include('Game score: deuce')
    end
  end

  context 'using keyboard controls' do
    def feed_keys(sequence)
      StringIO.new sequence.split.join("\n")
    end

    it 'should react to F key' do
      game.input = feed_keys('f')
      game.start
      expect(game.output.string).to include('Federer: 1')
    end
    
    it 'should react to M key' do
      game.input = feed_keys('m')
      game.start
      expect(game.output.string).to include('McEnroe: 1')
    end

    context 'playing score sequences' do
      it 'should increment score' do
        game.input = feed_keys('m m')
        game.start
        expect(game.output.string).to include('McEnroe: 2')
      end
            
      it 'should recognize McEnroe win' do
        game.input = feed_keys('m m m m')
        game.start
        expect(game.output.string).to include('McEnroe: 4')
        expect(game.output.string).to include('win')
      end

      it 'should recognize Federer win' do
        game.input = feed_keys('f f f f')
        game.start
        expect(game.output.string).to include('Federer: 4')
        expect(game.output.string).to include('win')
      end
      
      it 'should recognize deuce score' do
        game.input = feed_keys('m m f f f m m')
        game.start
        expect(game.output.string).not_to include('win')
        expect(game.output.string).to include('Game score: deuce')
      end

      it 'should allow game to go on' do
        game.input = feed_keys('m m f f f m m f m f m f m')
        game.start
        expect(game.output.string).not_to include('win')
        expect(game.output.string).to include('Game score: deuce')
        expect(game.output.string).to include('Federer: 6')
        expect(game.output.string).to include('McEnroe: 7')
      end

      it 'should report score: fifteen-love' do
        game.input = feed_keys('f')
        game.start
        expect(game.output.string).to include('Game score: fifteen-love')
      end

      it 'should report score: forty-thirty' do
        game.input = feed_keys('f f f m m')
        game.start
        expect(game.output.string).to include('Game score: forty-thirty')
      end

      it 'should report score: fifteen-thirty' do
        game.input = feed_keys('f m m')
        game.start
        expect(game.output.string).to include('Game score: fifteen-thirty')
      end

      it 'should report score: advantage (Federer)' do
        game.input = feed_keys('f f f m m m f')
        game.start
        expect(game.output.string).to include('Game score: advantage')
      end

      it 'should report score: advantage (McEnroe)' do
        game.input = feed_keys('m m m f f f m f f')
        game.start
        expect(game.output.string).to include('Game score: advantage')
      end

      it 'should report score: win' do
        game.input = feed_keys('f f f m m m f m f f')
        game.start
        expect(game.output.string).to include('Game score: win')
      end

      it 'should report score: love-forty' do
        game.input = feed_keys('m m m')
        game.start
        expect(game.output.string).to include('Game score: love-forty')
      end

    end

  end

end

