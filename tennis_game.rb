require 'pry'

# The program plays a "game" of virtual tennis and continuously reports the score.
# Just run 'ruby tennis_game.rb start' and the game will start.
# You "play" with the keyboard by pressing F or M to score. Press Enter to exit.
#
# Author::    Fredrik Bränström  (mailto:fredrik@branstrom.name)
# Copyright:: Copyright (c) 2015 Fredrik Bränström
# License::   Distributes under the same terms as Ruby

module Tennis
  class Game
    attr_accessor :total_score_description, :input, :output

    def initialize
      self.input = $stdin
      self.output = $stdout

      @federer = Player.new
      @mcenroe = Player.new
    end
    
    def start
      output.puts 'The game has begun! Federer and McEnroe are facing off.'
      output.puts 'Press F or M to log a score for one of the players.'
      describe_score

      while prompt_user
        break if @total_score_description.include?('win')
      end
    end

    def prompt_user
      user_action = (input.gets || '').chomp
      next_move(user_action)
    end

    def next_move(user_action)
      case user_action.downcase
      when 'f' then point_won_by('federer')
      when 'm' then point_won_by('mcenroe')
      when '' then false
      else true
      end
    end

    def point_won_by(player)
      case player.downcase
      when 'federer'
        @federer.score += 1
      when 'mcenroe'
        @mcenroe.score += 1
      end
      describe_score
    end

    def describe_score
      @total_score_description =
        if highest_score >= 4 && score_diff >= 2
          'win'
        elsif scores_over_three? && score_diff >= 1
          'advantage'
        elsif scores_over_three? && equal_scores?
          'deuce'
        elsif equal_scores?
          "#{@federer.score_description}-all"
        else
          "#{@federer.score_description}-#{@mcenroe.score_description}"
        end

      announce_score
      @total_score_description
    end
    
    def announce_score
      output.puts "Game score: #{@total_score_description}"
      output.puts "Federer: #{@federer.score} McEnroe: #{@mcenroe.score}"
    end
    
    def player_scores
      [@federer.score, @mcenroe.score]
    end
    
    def highest_score
      player_scores.max
    end

    def lowest_score
      player_scores.min
    end

    def score_diff
      highest_score - lowest_score
    end
    
    def equal_scores?
      @federer.score == @mcenroe.score
    end
    
    def scores_over_three?
      highest_score >= 3 && lowest_score >= 3
    end
  end

  class Player
    attr_accessor :score

    def initialize
      @score = 0
    end

    def score_description
      case @score
      when 0 then 'love'
      when 1 then 'fifteen'
      when 2 then 'thirty'
      when 3 then 'forty'
      else @score
      end
    end
  end
end

if ARGV.shift == 'start'
  game = Tennis::Game.new
  game.start
end
